namespace TgJobsApi.Models
{
    using System;
    using System.Collections.Generic;

    public partial class SalesOrderHeader
    {
        public SalesOrderHeader()
        {
            this.SalesOrderDetails = new List<SalesOrderDetail>();
        }

        public SalesOrderHeader(int Id, int CustomerId, DateTime OrderDate, DateTime? ShipDate, byte Status, double Total) 
            : this()
        {
            this.SalesOrderID = Id;
            this.CustomerID = CustomerId;
            this.OrderDate = OrderDate;
            this.ShipDate = ShipDate;
            this.Status = Status;
            this.Total = Total;
        }

        public int SalesOrderID { get; set; }

        public int CustomerID { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? ShipDate { get; set; }

        /// <summary>
        /// 1. Pending
        /// 2. Sent
        /// 3. Complete
        /// </summary>
        public byte Status { get; set; }

        public double Total { get; set; }

        public virtual ICollection<SalesOrderDetail> SalesOrderDetails { get; set; }
    }
}
