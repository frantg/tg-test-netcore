﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using TgJobsApi.Helpers;
using TgJobsApi.Models;
using System;

namespace TgJobsApi.Controllers
{
    [ApiController, Produces("application/json")]
    [Route("api/")]
    public class ValuesController : ControllerBase
    {
        private readonly List<Customer> customers;
        private readonly List<Product> products;
        private readonly List<SalesOrderHeader> sales;

        public ValuesController()
        {
            this.customers = CustomerHelper.generateCustomers();
            this.products = ProductHelper.generateProducts();
            this.sales = SaleHelper.getSales();
        }

        /// <summary>
        /// Endpoint para obtener un customer
        /// </summary>
        /// <returns>Un objeto Customer</returns>
        [HttpGet("getCustomer/{idCustomer}")]
        public IActionResult GetCustomer(int idCustomer)
        {
            Customer customer = this.customers.FirstOrDefault(c => c.CustomerID == idCustomer);
            return Ok(customer);
        }

        /// <summary>
        /// Endpoint para obtener las compras dado un customerId
        /// </summary>
        /// <returns>Listado de SalesOrderHeader</returns>
        [HttpGet("getSalesByCustomer/{idCustomer}/")]
        public IActionResult GetSalesByCustomer(int idCustomer)
        {
            List<SalesOrderHeader> listSales = this.sales.Where(s => s.CustomerID == idCustomer).ToList();
            return Ok(listSales);
        }

        /// <summary>
        /// Endpoint para obtener el producto más vendido
        /// </summary>
        /// <returns>Un objeto Product</returns>
        [HttpGet("getTopProduct")]
        public IActionResult GetTopProduct()
        {
            return BadRequest(new NotImplementedException());
        }

        /// <summary>
        /// Endpoint para obtener los clientes y sus prodcutos comprados
        /// </summary>
        /// <returns>Listado de customer con array de products</returns>
        [HttpGet("getCustomerProducts")]
        public IActionResult GetCustomerProducts()
        {
            return BadRequest(new NotImplementedException());
        }

        /// <summary>
        /// Endpoint para obtener la venta más pesada (la suma de los pesos de los productos
        /// </summary>
        /// <returns>Un objeto SalesOrderHeader</returns>
        [HttpGet("getWeightestSale")]
        public IActionResult GetWeightestSale()
        {
            return BadRequest(new NotImplementedException());
        }
    }
}
